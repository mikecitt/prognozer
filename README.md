# Prognozer - Projektni zadatak iz HCI

**Milan Pavlov - SW35/2017**


Korišćene tehnologije: 

*  HTML
*  CSS ([Bootstrap](https://getbootstrap.com/))
*  jQuery ([canvasJS](https://canvasjs.com/) za iscrtavanje grafika)

Podaci preuzeti sa [OpenWeatherMap](https://openweathermap.org/) korišćenjem [One Call API](https://openweathermap.org/api/one-call-api) uz upotrebu AJAX-a.


Rešenje se pokreće startovanjem Web Servera:

```
Windows: start-server.bat
Mac OS / Linux: start-server.sh
(Python 3)

Ručno: 
Python 3: python -m http.server 8000
Python 2: python -m SimpleHTTPServer 8000


```
Potom otvaranjem početne stranice na adresi http://localhost:8000

*Za ovaj način startovanja Web Servera potrebno je imati instaliran Python, ali mogu se koristiti i neki drugi načini za pokretanje Web Servera.*