function toggleModal() {
  $("#modal").modal('toggle')
}

$(document).ready(function() {
  $("#modal").modal('toggle');
});

CanvasJS.addCultureInfo("sr",
  {
    shortDays: ["pon", "uto", "sre", "čet", "pet", "sub", "ned"]

  });

var options = {
  animationEnabled: true,
  animationDuration: 1000,
  backgroundColor: null,
  theme: "light2",
  zoomEnabled: true,
  culture: "sr",
  panEnabled: true,
  legend: {
            cursor: "pointer",
            itemclick: function (e) {
                //console.log("legend click: " + e.dataPointIndex);
                //console.log(e);
                if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                    e.dataSeries.visible = false;
                } else {
                    e.dataSeries.visible = true;
                }

                e.chart.render();
            }
        },
  toolTip: {
    enabled: true,
    shared: true
  },
  title: {
    text: ""
  },
  axisX: {
    title: "Vreme merenja",
    valueFormatString: "DDD HH:mm" // TODO: srediti prikaz 7-dnevne temp (sredina dana)
  },
  axisY: {
    titleFontSize: 24,
    includeZero: false
  },
  data: []
};

function posalji() {
  $("#accordion").empty();
  options.data = [];
  $("#modal").modal('hide');
  var tags = $("#tags").tagsinput('items');
  var lowerCaseTags = $.map(tags, function(n,i){return n.toLowerCase();});
  var found = [];
  var foundNames = [];
  $.getJSON('data/city.list.json', function(data) {
    console.log(data);
    $.each(data, function(key, value) {
      if($.inArray(value.name.toLowerCase(), lowerCaseTags) != -1 & value.country == "RS") {
        found.push(value);
      }
    });
    if (found.length < tags.length) {
      alert("Neki gradovi nisu pronađeni i biće obrisani sa liste.");
    }
    $('#tags').tagsinput('removeAll');
    $.each(found, function(key, value) {
      $("#tags").tagsinput("add", value.name);
    });
    dodaj(found);
    $("#chartContainer").CanvasJSChart(options);
    $("#table").show();
  });
}

function dodaj(found) {
  $.each(found, function(key, city) {
    $.ajax({
      url: "https://api.openweathermap.org/data/2.5/onecall?lat=" + city.coord.lat + "&lon=" + city.coord.lon + "&appid=39314e0ae6d3990ee9fa61eca7f546a9",
      type: "GET",
      dataType: "json",
      async: false,
      complete: function (data) {
        console.log(data.responseJSON);
        $("#div").append("");
        dataPoints = [];
        console.log(city);
        var card =
        `
        <div class="card">
          <div class="card-header" id="$id{city.id}-header">
            <h5 class="mb-0">
              <button class="btn btn-link" data-toggle="collapse" data-target="#id${city.id}" aria-expanded="true" aria-controls="id${city.id}">
                ${city.name} (${city.country})
              </button>
            </h5>
          </div>
          <div id="id${city.id}" class="collapse" aria-labelledby="id${city.id}-header" data-parent="#accordion">
            <div class="card-body table-responsive-lg">
              <div class="tablee">
                <table class="table table-hover">
                  <thead class="thead-dark">
                      <tr>
                          <th>Vreme merenja</th>
                          <th>Temp</th>
                          <th>Max</th>
                          <th>Min</th>
                          <th>Pritisak</th>
                          <th>Vlažnost</th>
                      </tr>
                  </thead>
                  <tbody id="id${city.id}-tbody">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        `;
        $("#accordion").append(card);

        if ($("#tip").val() == "1") {
          options.axisX.valueFormatString = "DDD HH:mm";
          $.each(data.responseJSON.hourly, function (index, value) {
            if (index > 6) {
              return false;
            }

            if ($("#podatak").val() == "1") {
              dataPoints.push({
                x: new Date(value.dt*1000),
                y: parseInt((value.temp - 273.15).toFixed(2))
              });
            } else if($("#podatak").val() == "2"){
              dataPoints.push({
                x: new Date(value.dt*1000),
                y: parseInt(value.pressure)
              });
            } else {
              dataPoints.push({
                x: new Date(value.dt*1000),
                y: parseInt(value.humidity)
              });
            }

            var row =
              `
            <tr>
              <td>${$.format.date(new Date(value.dt*1000), "dd.MM.yyyy. HH:mm")}</td>
              <td>${(value.temp - 273.15).toFixed(2)}°C</td>
              <td>${(value.temp - 273.15).toFixed(2)}°C</td>
              <td>${(value.temp - 273.15).toFixed(2)}°C</td>
              <td>${value.pressure} hPa</td>
              <td>${value.humidity}%</td>
            </tr>
            `;
            $('#id'+city.id+'-tbody').append(row);
          });
        } else if ($("#tip").val() == "2") {
          options.axisX.valueFormatString = "DDD HH:mm";
          $.each(data.responseJSON.hourly, function (index, value) {

            if ($("#podatak").val() == "1") {
              dataPoints.push({
                x: new Date(value.dt*1000),
                y: parseInt((value.temp - 273.15).toFixed(2))
              });
            } else if($("#podatak").val() == "2"){
              dataPoints.push({
                x: new Date(value.dt*1000),
                y: parseInt(value.pressure)
              });
            } else {
              dataPoints.push({
                x: new Date(value.dt*1000),
                y: parseInt(value.humidity)
              });
            }

            var row =
              `
            <tr>
              <td>${$.format.date(new Date(value.dt*1000), "dd.MM.yyyy. HH:mm")}</td>
              <td>${(value.temp - 273.15).toFixed(2)}°C</td>
              <td>${(value.temp - 273.15).toFixed(2)}°C</td>
              <td>${(value.temp - 273.15).toFixed(2)}°C</td>
              <td>${value.pressure} hPa</td>
              <td>${value.humidity}%</td>
            </tr>
            `;
            $('#id'+city.id+'-tbody').append(row);
          });
        } else {
          options.axisX.valueFormatString = "DDD";
          $.each(data.responseJSON.daily, function (index, value) {

            if ($("#podatak").val() == "1") {
              dataPoints.push({

                x: new Date(value.dt*1000 - 43200000),
                y: parseInt((value.temp.day - 273.15).toFixed(2))
              });
            } else if($("#podatak").val() == "2"){
              dataPoints.push({
                x: new Date(value.dt*1000 - 43200000),
                y: parseInt(value.pressure)
              });
            } else {
              dataPoints.push({
                x: new Date(value.dt*1000 - 43200000),
                y: parseInt(value.humidity)
              });
            }

            var row =
              `
            <tr>
              <td>${$.format.date(new Date(value.dt*1000), "dd.MM.yyyy.")}</td>
              <td>${(value.temp.day - 273.15).toFixed(2)}°C</td>
              <td>${(value.temp.max - 273.15).toFixed(2)}°C</td>
              <td>${(value.temp.min - 273.15).toFixed(2)}°C</td>
              <td>${value.pressure} hPa</td>
              <td>${value.humidity}%</td>
            </tr>
            `;
            $('#id'+city.id+'-tbody').append(row);
          });
        }



        var data = {
          type: "spline",
          xValueType: "dateTime",
          xvalueFormatString: "DDD HH:mm",
          showInLegend: true,
          legendText: city.name + " (" + city.country + ")",
          name: city.name + " (" + city.country + ")",
          dataPoints: dataPoints
        }
        console.log(data);

        if ($("#podatak").val() == "1") {
          options.axisY.title = "Temperatura";
          options.axisY.valueFormatString = "# °C";
          data.yValueFormatString = "# °C";
        } else if($("#podatak").val() == "2"){
          options.axisY.title = "Vazdušni pritisak";
          options.axisY.valueFormatString = "# hPa",
          data.yValueFormatString = "# hPa";
        } else {
          options.axisY.title = "Vlažnost vazduha";
          options.axisY.valueFormatString= "#'%'";
          data.yValueFormatString = "#'%'";
        }

        if ($("#tip").val() == "3") {
          data.xValueFormatString = "DD.MM.YYYY.";
        }
        else {
          data.xValueFormatString = "DD.MM.YYYY. HH:mm";
        }
        options.data.push(data);

        console.log(options);
      }
    });
  });
}
